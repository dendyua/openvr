set(TARGET_NAME driver_sample)

add_library(${TARGET_NAME} SHARED
  driverlog.cpp
  driverlog.h
  driver_sample.cpp
)

add_definitions(-DDRIVER_SAMPLE_EXPORTS)

target_link_libraries(${TARGET_NAME}
  ${OPENVR_LIBRARIES}
  ${CMAKE_DL_LIBS}
)

set_target_properties(${TARGET_NAME} PROPERTIES
	PREFIX ""
	LIBRARY_OUTPUT_DIRECTORY "/home/dendy/projects/ecstatica/src/libs/openvr/samples/bin/drivers/sample/linux64"
)

setTargetOutputDirectory(${TARGET_NAME})
